package br.com.itau.ApacheCamel;

import org.apache.camel.CamelContext;
import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.impl.DefaultCamelContext;

public class IntegracaoArquivos {

	public static void main(String[] args) throws Exception {
		CamelContext contexto = new DefaultCamelContext();
		
		contexto.addRoutes(new RouteBuilder() {
			
			@Override
			public void configure() throws Exception {
			
				errorHandler(defaultErrorHandler().maximumRedeliveries(3)
			   .redeliveryDelay(2000).onExceptionOccurred(new Processor() {
					
				@Override
				public void process(Exchange exchange) throws Exception {
					String arquivo = (String) exchange.getIn().getHeader(Exchange.FILE_NAME, String.class);
					System.out.printf("\nDeu ruim na cópia %s", arquivo);
					System.out.println("Erro: " + exchange.getException().getMessage());
				}
			}));
				
				
			from("file://origem/?recursive=true&noop=true")
			.process(new Processor() {
			
			@Override
			public void process(Exchange exchange) throws Exception{
				String nomeArquivo = (String) exchange.getIn().getHeader(Exchange.FILE_NAME, String.class);
				System.out.printf("\nTentando copiar arquivo %s", nomeArquivo);
			}
		})
			.to("file://destino/")
			.delay(10_000)
			.end();
			}
		});
		
		contexto.start();
		
		for (;;) {}
	}
}